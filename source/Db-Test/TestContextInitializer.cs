﻿using System;
using System.Data.Entity;
using Db_Test.Model;

namespace Db_Test
{
    public class TestContextInitializer : CreateDatabaseIfNotExists<TestContext>
    {
        protected override void Seed(TestContext context)
        {
            Console.WriteLine("Creating database");

            context.People.Add(new Person { Name = "Günther", YoB = 1982 });
            context.People.Add(new Person { Name = "Isabella", YoB = 1994 });
            context.SaveChanges();
        }
    }
}