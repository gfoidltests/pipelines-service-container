﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using Db_Test.Model;

namespace Db_Test
{
    static class Program
    {
        private static readonly TestContext _db = new TestContext();
        //---------------------------------------------------------------------
        static void Main()
        {
            try
            {
                int personCount = Run();

                Environment.ExitCode = personCount > 0 ? 0 : 1;
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex);
                Console.ResetColor();

                Environment.ExitCode = 2;
            }

            Console.WriteLine("\nbye");
        }
        //---------------------------------------------------------------------
        private static int Run()
        {
            WaitForDb();

            var persons = _db.People.OrderBy(p => p.YoB).ToList();

            Console.WriteLine();
            Console.WriteLine($"{"Name",-20} {"YoB",-5}");
            Console.WriteLine(new string('-', 25));
            foreach (var person in persons)
                Console.WriteLine($"{person.Name,-20} {person.YoB,-5}");

            return persons.Count;
        }
        //---------------------------------------------------------------------
        private static void WaitForDb()
        {
            int trialCounter = 0;
            string connString = ConfigurationManager.ConnectionStrings["TestContext"].ConnectionString.Replace("Test", "master");
            SqlConnection conn = new SqlConnection(connString);
            try
            {
                do
                {
                    Console.WriteLine($"Waiting for db...try # {++trialCounter}");
                    Thread.Sleep(1000);
                    try
                    {
                        conn.Open();
                        return;
                    }
                    catch (SqlException)
                    {
                        continue;
                    }
                } while (trialCounter < 30);

                Environment.Exit(1);
            }
            finally
            {
                conn.Dispose();
            }
        }
    }
}