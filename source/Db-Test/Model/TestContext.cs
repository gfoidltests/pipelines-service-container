﻿using System.Data.Entity;

namespace Db_Test.Model
{
    public class TestContext : DbContext
    {
        public TestContext() : base("TestContext")
        {
            Database.SetInitializer<TestContext>(new TestContextInitializer());
        }
        //---------------------------------------------------------------------
        public virtual DbSet<Person> People { get; set; }
    }
}