﻿namespace Db_Test.Model
{
    public class Person
    {
        public int Id      { get; set; }
        public string Name { get; set; }
        public int YoB     { get; set; }
    }
}